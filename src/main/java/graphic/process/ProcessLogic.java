package graphic.process;

import graphic.engine.*;
import graphic.shader.Camera;
import graphic.shader.Material;
import graphic.shader.Mesh;
import graphic.shader.Texture;
import graphic.shader.light.DirectionalLight;
import graphic.shader.light.PointLight;
import graphic.shader.light.SpotLight;
import org.joml.Vector2f;
import org.joml.Vector3f;

import static org.lwjgl.glfw.GLFW.*;

public class ProcessLogic implements IProcessLogic {

    private static final float MOUSE_SENSITIVITY = 0.2f;
    private static final float CAMERA_POS_STEP = 0.05f;
    private final Vector3f cameraInc;
    private final Camera camera;
    private Item[] items;
    Item cubeItem;
    private final Renderer renderer;
    private Scene scene;
    private float lightAngle;
    private float angleInc;

    public ProcessLogic() {
        renderer = new Renderer();
        camera = new Camera();
        cameraInc = new Vector3f(0, 0, 0);
        lightAngle = -90;
        angleInc = 0;
    }

    @Override
    public void init(Window window) throws Exception {
        renderer.init(window);

        scene = new Scene();

        // Setup  Items

        items = new Item[]{ setupCat(), setupQuad(), setupStatue(), setupCube() };
        scene.setItems(items);

        // Setup  SkyBox
        /*WorldBox skyBox = new WorldBox("/models/worldBox.obj", "/textures/devilmap.png");
        skyBox.setScale(500.0f);
        scene.setWorldBox(skyBox);*/

        // Setup Lights
        setupLights();

        camera.getPosition().z = 10;
        camera.getPosition().y = 1;
    }

    private Item setupStatue() throws Exception {
        float reflectance = 1f;
        Mesh statueMesh = ObjLoader.loadMesh("/models/statue.obj");
        Texture statueTexture = new Texture("/textures/statue.png");
        Material statueMaterial = new Material(statueTexture, reflectance);
        statueMesh.setMaterial(statueMaterial);
        Item statue = new Item(statueMesh);
        statue.setPosition(-4, -1, -2);
        statue.setScale(1f);
        return statue;
    }

    private Item setupCube() throws Exception {
        float reflectance = 10f;
        Mesh cubeMesh = ObjLoader.loadMesh("/models/cube.obj");
        Texture cubeTexture = new Texture("/textures/steel.png");
        Material cubeMaterial = new Material(cubeTexture, reflectance);
        cubeMesh.setMaterial(cubeMaterial);
        cubeItem = new Item(cubeMesh);
        cubeItem.setPosition(0, 0, 0);
        cubeItem.setScale(1.0f);
        return cubeItem;
    }

    private Item setupQuad() throws Exception {
        float reflectance = 1f;
        Mesh quadMesh = ObjLoader.loadMesh("/models/plane.obj");
        Texture quadTexture = new Texture("/textures/grass.png");
        Material quadMaterial = new Material(quadTexture, reflectance);
        quadMesh.setMaterial(quadMaterial);
        Item quadItem = new Item(quadMesh);
        quadItem.setPosition(0, -1, 0);
        quadItem.setScale(10.0f);
        return quadItem;
    }

    private Item setupCat() throws Exception {
        float reflectance = 3f;
        Mesh catMesh = ObjLoader.loadMesh("/models/cat.obj");
        Texture catTexture = new Texture("/textures/cat_diff.png");
        Material catMaterial = new Material(catTexture, reflectance);
        catMesh.setMaterial(catMaterial);
        Item cat = new Item(catMesh);
        cat.setPosition(3, -1, 3);
        cat.setScale(2f);
        return cat;
    }

    private void setupLights() {
        SceneLight sceneLight = new SceneLight();
        scene.setSceneLight(sceneLight);

        // Ambient Light
        sceneLight.setAmbientLight(new Vector3f(0.3f, 0.3f, 0.3f));
        sceneLight.setWorldBoxLight(new Vector3f(1.0f, 1.0f, 1.0f));

        //Point light
        Vector3f lightPosition = new Vector3f(0, 1f, -1f);
        float lightIntensity = 5.0f;
        PointLight pointLight = new PointLight(new Vector3f(0, 1, 1), lightPosition, lightIntensity);
        PointLight.Attenuation att = new PointLight.Attenuation(0.0f, 0.0f, 1.0f);
        pointLight.setAttenuation(att);
        sceneLight.setPointLightList(new PointLight[]{pointLight});

        // Spot Light
        lightIntensity = 50.0f;
        lightPosition = new Vector3f(0f, 0.0f, 100f);
        pointLight = new PointLight(new Vector3f(1, 0, 1), lightPosition, lightIntensity);
        att = new PointLight.Attenuation(0.0f, 0.0f, 0.02f);
        pointLight.setAttenuation(att);
        Vector3f coneDir = new Vector3f(0, 0, -1);
        float cutoff = (float) Math.cos(Math.toRadians(140));
        SpotLight spotLight = new SpotLight(pointLight, coneDir, cutoff);
        sceneLight.setSpotLightList(new SpotLight[]{spotLight, new SpotLight(spotLight)});

        // Directional Light
        lightIntensity = 1.0f;
        Vector3f lightDirection = new Vector3f(0, 1, 1);
        DirectionalLight directionalLight = new DirectionalLight(new Vector3f(1, 1, 1), lightDirection, lightIntensity);
        directionalLight.setShadowPosMult(5);
        directionalLight.setOrthoCords(-10.0f, 10.0f, -10.0f, 10.0f, -10.0f, 20.0f);
        sceneLight.setDirectionalLight(directionalLight);
    }

    @Override
    public void input(Window window, MouseInput mouseInput) {
        cameraInc.set(0, 0, 0);
        if (window.isKeyPressed(GLFW_KEY_W)) {
            cameraInc.z = -1;
        } else if (window.isKeyPressed(GLFW_KEY_S)) {
            cameraInc.z = 1;
        }
        if (window.isKeyPressed(GLFW_KEY_A)) {
            cameraInc.x = -1;
        } else if (window.isKeyPressed(GLFW_KEY_D)) {
            cameraInc.x = 1;
        }
        if (window.isKeyPressed(GLFW_KEY_Z)) {
            cameraInc.y = -1;
        } else if (window.isKeyPressed(GLFW_KEY_X)) {
            cameraInc.y = 1;
        }
        if (window.isKeyPressed(GLFW_KEY_LEFT)) {
            angleInc -= 0.05f;
        } else if (window.isKeyPressed(GLFW_KEY_RIGHT)) {
            angleInc += 0.05f;
        } else {
            angleInc = 0;
        }
        float lightPos = scene.getSceneLight().getSpotLightList()[0].getPointLight().getPosition().z;
        if (window.isKeyPressed(GLFW_KEY_N)) {
            scene.getSceneLight().getSpotLightList()[0].getPointLight().getPosition().z = lightPos + 0.1f;
        } else if (window.isKeyPressed(GLFW_KEY_M)) {
            scene.getSceneLight().getSpotLightList()[0].getPointLight().getPosition().z = lightPos - 0.1f;
        }
    }

    @Override
    public void update(float interval, MouseInput mouseInput) {
        // Update camera based on mouse
        if (mouseInput.isRightButtonPressed()) {
            Vector2f rotVec = mouseInput.getDisplVec();
            camera.moveRotation(rotVec.x * MOUSE_SENSITIVITY, rotVec.y * MOUSE_SENSITIVITY, 0);
        }

        // Update camera position
        Vector3f prevPos = new Vector3f(camera.getPosition());
        camera.movePosition(cameraInc.x * CAMERA_POS_STEP, cameraInc.y * CAMERA_POS_STEP, cameraInc.z * CAMERA_POS_STEP);

        float rotY = cubeItem.getRotation().y;
        rotY += 0.5f;
        if ( rotY >= 360 ) {
            rotY -= 360;
        }
        cubeItem.getRotation().y = rotY;

        lightAngle += angleInc;
        if ( lightAngle < 0 ) {
            lightAngle = 0;
        } else if (lightAngle > 180 ) {
            lightAngle = 180;
        }
        float zValue = (float)Math.cos(Math.toRadians(lightAngle));
        float yValue = (float)Math.sin(Math.toRadians(lightAngle));
        Vector3f lightDirection = this.scene.getSceneLight().getDirectionalLight().getDirection();
        lightDirection.x = 0;
        lightDirection.y = yValue;
        lightDirection.z = zValue;
        lightDirection.normalize();
    }

    @Override
    public void render(Window window) throws Exception {
        renderer.render(window, camera, scene);
    }

    @Override
    public void cleanup() {
        renderer.cleanup();
        for (Item item : items) {
            item.getMesh().cleanUp();
        }
    }
}
