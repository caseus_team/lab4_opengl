package graphic.engine;

import graphic.shader.light.DirectionalLight;
import graphic.shader.light.PointLight;
import graphic.shader.light.SpotLight;
import org.joml.Vector3f;

public class SceneLight {

    private Vector3f ambientLight;

    private Vector3f worldBoxLight;

    private PointLight[] pointLightList;

    private SpotLight[] spotLightList;

    private DirectionalLight directionalLight;

    public Vector3f getAmbientLight() {
        return ambientLight;
    }

    public void setAmbientLight(Vector3f ambientLight) {
        this.ambientLight = ambientLight;
    }

    public PointLight[] getPointLightList() {
        return pointLightList;
    }

    public void setPointLightList(PointLight[] pointLightList) {
        this.pointLightList = pointLightList;
    }

    public SpotLight[] getSpotLightList() {
        return spotLightList;
    }

    public void setSpotLightList(SpotLight[] spotLightList) {
        this.spotLightList = spotLightList;
    }

    public DirectionalLight getDirectionalLight() {
        return directionalLight;
    }

    public void setDirectionalLight(DirectionalLight directionalLight) {
        this.directionalLight = directionalLight;
    }

    public Vector3f getWorldBoxLight() {
        return worldBoxLight;
    }

    public void setWorldBoxLight(Vector3f worldBoxLight) {
        this.worldBoxLight = worldBoxLight;
    }

}
