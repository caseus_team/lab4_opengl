package graphic.engine;

import graphic.process.MouseInput;

public class Engine implements Runnable {

    public static final int TARGET_FPS = 75;

    public static final int TARGET_UPS = 30;
    private final Window window;
    private final Thread loopThread;
    private final Timer timer;
    private final IProcessLogic processLogic;
    private final MouseInput mouseInput;


    public Engine(String windowTitle, int width, int height, boolean vSync, IProcessLogic processLogic) {
        loopThread = new Thread(this, "GAME_LOOP_THREAD");
        window = new Window(windowTitle, width, height, vSync);
        mouseInput = new MouseInput();
        this.processLogic = processLogic;
        timer = new Timer();
    }

    public void start() {
        String osName = System.getProperty("os.name");
        if ( osName.contains("Mac") ) {
            loopThread.run();
        } else {
            loopThread.start();
        }
    }

    @Override
    public void run() {
        try {
            init();
            loop();
        } catch (Exception excp) {
            excp.printStackTrace();
        } finally {
            processLogic.cleanup();
        }
    }

    protected void init() throws Exception {
        window.init();
        timer.init();
        mouseInput.init(window);
        processLogic.init(window);
    }

    protected void loop() throws Exception {
        float elapsedTime;
        float accumulator = 0f;
        float interval = 1f / TARGET_UPS;

        boolean running = true;
        while (running && !window.windowShouldClose()) {
            elapsedTime = timer.getElapsedTime();
            accumulator += elapsedTime;

            input();

            while (accumulator >= interval) {
                update(interval);
                accumulator -= interval;
            }

            render();

            if (!window.isvSync()) {
                sync();
            }
        }
    }

    private void sync() {
        float loopSlot = 1f / TARGET_FPS;
        double endTime = timer.getLastLoopTime() + loopSlot;
        while (timer.getTime() < endTime) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException ie) {
            }
        }
    }

    protected void input() {
        mouseInput.input(window);
        processLogic.input(window, mouseInput);
    }

    protected void update(float interval) {
        processLogic.update(interval, mouseInput);
    }

    protected void render() throws Exception {
        processLogic.render(window);
        window.update();
    }
}
