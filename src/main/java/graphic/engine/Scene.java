package graphic.engine;

import graphic.shader.Mesh;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Scene {

    private Map<Mesh, List<Item>> meshMap;

    private WorldBox worldBox;

    private SceneLight sceneLight;

    public Scene() {
        meshMap = new HashMap();
    }

    public Map<Mesh, List<Item>> getGameMeshes() {
        return meshMap;
    }

    public void setItems(Item[] items) {
        // Create a map of meshes to speed up rendering
        int numItems = items != null ? items.length : 0;
        for (int i = 0; i < numItems; i++) {
            Item item = items[i];
            Mesh[] meshes = item.getMeshes();
            for (Mesh mesh : meshes) {
                List<Item> list = meshMap.get(mesh);
                if (list == null) {
                    list = new ArrayList<>();
                    meshMap.put(mesh, list);
                }
                list.add(item);
            }
        }
    }

    public void cleanup() {
        for (Mesh mesh : meshMap.keySet()) {
            mesh.cleanUp();
        }
    }

    public WorldBox getWorldBox() {
        return worldBox;
    }

    public void setWorldBox(WorldBox worldBox) {
        this.worldBox = worldBox;
    }

    public SceneLight getSceneLight() {
        return sceneLight;
    }

    public void setSceneLight(SceneLight sceneLight) {
        this.sceneLight = sceneLight;
    }

}
