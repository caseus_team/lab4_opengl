package graphic.engine;

import graphic.process.ObjLoader;
import graphic.shader.Material;
import graphic.shader.Mesh;
import graphic.shader.Texture;

public class WorldBox extends Item {

    public WorldBox(String objModel, String textureFile) throws Exception {
        super();
        Mesh skyBoxMesh = ObjLoader.loadMesh(objModel);
        Texture skyBoxtexture = new Texture(textureFile);
        skyBoxMesh.setMaterial(new Material(skyBoxtexture, 0.0f));
        setMesh(skyBoxMesh);
        setPosition(0, 0, 0);
    }

}
