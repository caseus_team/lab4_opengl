package graphic.engine;

import graphic.process.MouseInput;

public interface IProcessLogic {

    void init(Window window) throws Exception;

    void input(Window window, MouseInput mouseInput);

    void update(float interval, MouseInput mouseInput);

    void render(Window window) throws Exception;

    void cleanup();
}
