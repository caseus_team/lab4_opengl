package graphic;

import graphic.engine.Engine;
import graphic.engine.IProcessLogic;
import graphic.process.ProcessLogic;

public class ShopWindow {

    public static void main(String[] args) {
        try {
            boolean vSync = true;
            IProcessLogic gameLogic = new ProcessLogic();
            Engine gameEng = new Engine("Shop", 600, 480, vSync, gameLogic);
            gameEng.start();
        } catch (Exception excp) {
            excp.printStackTrace();
            System.exit(-1);
        }
    }

}
